class AddRewardCodeToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :rewards_code, :string
  end
end
