class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :product
      t.integer :quantity
      t.decimal :price
      t.string :cc
      t.string :cvv
      t.string :expiration
      t.string :first_name
      t.string :last_name


      t.timestamps
    end
  end
end
