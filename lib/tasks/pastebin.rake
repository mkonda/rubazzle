namespace :pastebin do
  desc 'creates pastebin entry for talk'
  task :create => :environment do
    File.open("passwords.txt", "w+") do |f|
      50.times do
        f.puts "#{Faker::Internet.email} : #{Digest::MD5.hexdigest(SecureRandom.random_bytes)}"
      end
    end
  end
end
