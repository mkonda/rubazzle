# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    product "MyString"
    quantity 1
    price ""
    cc "MyString"
    cvv "MyString"
    expiration "MyString"
    first_name "MyString"
    last_name "MyString"
  end
end
