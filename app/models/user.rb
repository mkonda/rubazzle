require Rails.root.join('lib', 'devise', 'encryptors', 'md5')

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable 
         #,:encryptable, :encryptor => :md5

  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  validates_format_of :rewards_code, :with => /^[a-z0-9]+$/i, :multiline => true, :allow_blank => true

  def set_default_role
    self.role ||= :user
  end

#def password_salt
#end

#def password_salt=(new_salt)
#end

#  def valid_password?(password)
#  	puts "password #{password} encrypted password #{encrypted_password}"
#  	puts Devise::Encryptable::Encryptors::Md5.digest("password", nil, nil, nil)
#  	return false if ! encrypted_password
#  	return false if encrypted_password.blank?
#  	Devise.secure_compare(Devise::Encryptable::Encryptors::Md5.digest(password, nil, nil, nil), self.encrypted_password)
#  end

  protected
	def confirmation_required?
    	false
	end
end
