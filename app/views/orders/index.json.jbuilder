json.array!(@orders) do |order|
  json.extract! order, :id, :product, :quantity, :price, :cc, :cvv, :expiration, :first_name, :last_name
  json.url order_url(order, format: :json)
end
