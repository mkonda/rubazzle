def update
  @user = User.find(params[:id])
  authorize @user
  if @user.update_attributes(params.permit!)
    redirect_to users_path, :notice => "User updated."
  else
    redirect_to users_path, :alert => "Unable to update user."
  end
end
