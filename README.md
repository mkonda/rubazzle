Rubazzle
=========

This application was created to deomonstrate key sets of 
vulnerabilities in combination for RailsConf 2014.


Getting Started
---------------



Documentation and Support
-------------------------

This is the only documentation.

#### Issues

Not a real application .. 

Similar Projects
----------------

Triage.

Contributing
------------

If you make improvements to this application, please share with others.

-   Fork the project on bitbucket.
-   Make your feature addition or bug fix.
-   Commit with Git.
-   Send the author a pull request.

If you add functionality to this application, create an alternative
implementation, or build an application that is similar, please contact
me and I’ll add a note to the README so that others can find your work.

Credits
-------

Aaron Bedra @abedra
Justin Collins @presidentbeef
Matthew Konda @mkonda


License
-------

BSD?



Development
-----------

-   Template Engine: ERB
-   Testing Framework: RSpec and Factory Girl and Cucumber
-   Front-end Framework: Bootstrap 3.0 (Sass)
-   Form Builder: SimpleForm
-   Authentication: Devise
-   Authorization: None
-   Admin: None

This application uses PostgreSQL with ActiveRecord.


Preferences:

* git: true
* apps4: none
* dev_webserver: webrick
* prod_webserver: same
* database: postgresql
* templates: erb
* unit_test: rspec
* integration: cucumber
* continuous_testing: guard
* fixtures: factory_girl
* frontend: bootstrap3
* email: gmail
* authentication: devise
* devise_modules: invitable
* authorization: pundit
* form_builder: simple_form
* starter_app: admin_app
* rvmrc: false
* quiet_assets: true
* local_env_file: figaro
* better_errors: true
* pry: true
* ban_spiders: true
* deployment: capistrano3


