Rubazzle::Application.routes.draw do
  resources :orders
  get 'specials' => 'orders#special'

  get 'before_after/compare'

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users
end